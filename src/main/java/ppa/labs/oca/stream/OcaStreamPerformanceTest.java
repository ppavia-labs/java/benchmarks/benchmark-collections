package ppa.labs.oca.stream;

import org.openjdk.jmh.annotations.Benchmark;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ppa.labs.oca.stream.model.Actor;
import ppa.labs.oca.stream.model.Decade;

import java.util.List;
import java.util.Set;

public class OcaStreamPerformanceTest {

    static final Logger logger = LoggerFactory.getLogger(OcaStreamPerformanceTest.class);
    static final Decade decade = new Decade(100, 1000, 2010);
    static final OcaStream ocaStream = new OcaStream();

    @Benchmark
    public void oldschoolCollectToList() {
        List<Integer> retour = ocaStream.oldschoolCollectToList(decade);
        logger.debug("mapReduceV1 " + retour.size());
    }

    @Benchmark
    public void streamFlatmapCollectToList() {
        List<Integer> retour = ocaStream.streamFlatmapCollectToList(decade);
        logger.debug("mapReduceV2 " + retour.size());
    }

    @Benchmark
    public void streamClosureCollectToSet() {
        Set<Actor> retour = ocaStream.streamClosureCollectToSet(decade);
        logger.debug("oneActorOfEachAgeV1 " + retour.size());
    }

    @Benchmark
    public void streamToMapCollectToSet() {
        Set<Actor> retour = ocaStream.streamToMapCollectToSet(decade);
        logger.debug("oneActorOfEachAgeV2 " + retour.size());
    }
    
    @Benchmark
    public void justForFunStreamToMapCollectToSet() {
        Set<Actor> retour = ocaStream.collectToMapThenSet(decade);
        logger.debug("oneActorOfEachAgeV2 " + retour.size());
    }
}
